apiVersion: v1
kind: Service
metadata:
  name: developers-entrepreneurs
  namespace: entrepreneurs
  labels:
    app.kubernetes.io/name: developers-entrepreneurs
    app.kubernetes.io/instance: developers-entrepreneurs-v1
    app.kubernetes.io/version: "v1"
    app.kubernetes.io/component: entrepreneurs-world-campaign
    app.kubernetes.io/part-of: developers
    app.kubernetes.io/managed-by: max
spec:
  ports:
    - name: "http-developers-entrepreneurs"
      port: 7092
      targetPort: 80
      protocol: "TCP"
  selector:
    app.kubernetes.io/name: developers-entrepreneurs
    app.kubernetes.io/instance: developers-entrepreneurs-v1
    app.kubernetes.io/version: "v1"
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: developers-entrepreneurs-v1
  namespace: entrepreneurs
  labels:
    app: developers-entrepreneurs-m4ReleaseImageTag()
    version: m4ReleaseImageTag()
    app.kubernetes.io/name: developers-entrepreneurs
    app.kubernetes.io/instance: developers-entrepreneurs-v1
    app.kubernetes.io/version: "v1"
    app.kubernetes.io/component: entrepreneurs-world-campaign
    app.kubernetes.io/part-of: developers
    app.kubernetes.io/managed-by: max
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: developers-entrepreneurs
      app.kubernetes.io/instance: developers-entrepreneurs-v1
      app.kubernetes.io/version: "v1"
  replicas: 1
  template:
    metadata:
      name: developers-entrepreneurs-v1
      labels:
        app.kubernetes.io/name: developers-entrepreneurs
        app.kubernetes.io/instance: developers-entrepreneurs-v1
        app.kubernetes.io/version: "v1"
    spec:
      imagePullSecrets:
        - name: regcred
      containers:
        - env:
            - name: VERSION_TAG
              value: m4ReleaseImageTag()
            - name: APP_BASEDIR_LOG
              value: /app/log
            - name: APP_BASEDIR_SRC
              value: /app/src
            - name: APP_BASEDIR_TMP
              value: /app/tmp
            - name: APP_DEBUG
              value: "0"
            - name: APP_ENV
              value: "prod"
            - name: APP_GROUP_ID
              value: "1000"
            - name: APP_PHP_MODULE_DIRECTORY
              value: /etc/php7/conf.d
            - name: APP_USER_ID
              value: "1000"
            - name: APP_WEBROOT
              value: /app/src/public
            - name: BASE_URL
              value: entrepreneurs.developersforfuture.org
            - name: COMPOSER_CACHE_DIR
              value: /build/cache/composeer
            - name: CONTAINER_DEBUG
              value: "0"
            - name: XDEBUG_ENABLED
              value: "0"
            - name: DB_HOST
              value: entrepreneurs-mysql
            - name: DB_USERNAME
              value: entrepreneurs
            - name: DB_DATABASE
              value: entrepreneurs
            - name: DB_PORT
              value: "3306"
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: db_password
                  name: entrepreneurs-db
            - name: MAILER_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: mailer_password
                  name: entrepreneurs-db
            - name: ADMIN_USER_PW
              valueFrom:
                secretKeyRef:
                  key: admin_user_password
                  name: entrepreneurs-db
            - name: PHPFPM_PM_MAX_CHILDREN
              value: "20"
          image: m4ReleaseImage():m4ReleaseImageTag()
          name: developers-entrepreneurs
          ports:
            - containerPort: 80
              name: "http"
          resources:
            requests:
              memory: "128Mi"
              cpu: "200m"
            limits:
              memory: "248Mi"
              cpu: "250m"
          readinessProbe:
            httpGet:
              path: /campaign-subscription
              port: 80
            initialDelaySeconds: 180
            timeoutSeconds: 1
            periodSeconds: 30
          livenessProbe:
            httpGet:
              path: /campaign-subscription
              port: 80
            initialDelaySeconds: 180
            timeoutSeconds: 1
            periodSeconds: 30
---
apiVersion: certmanager.k8s.io/v1alpha1
kind: Certificate
metadata:
  name: entrepreneurs-developersfor-org
  namespace: entrepreneurs
spec:
  organisation: DevelopersForFuture
  secretName: entrepreneurs-developersfor-org-tls
  renewBefore: 360h # 15d
  commonName: entrepreneurs.developersforfuture.org
  dnsNames:
    - entrepreneurs.developersforfuture.org
  issuerRef:
    name: letsencrypt-prod
    kind: ClusterIssuer
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: developersforentrepreneurs
  namespace: entrepreneurs
  annotations:
    kubernetes.io/ingress.class: "nginx"
    certmanager.k8s.io/cluster-issuer: letsencrypt-prod
    nginx.ingress.kubernetes.io/affinity: "cookie"
    nginx.ingress.kubernetes.io/session-cookie-name: "form"
    nginx.ingress.kubernetes.io/session-cookie-expires: "172800"
    nginx.ingress.kubernetes.io/session-cookie-max-age: "172800"
spec:
  tls:
    - hosts:
        - entrepreneurs.developersforfuture.org
      secretName: developersfor-org-tls
  rules:
    - host: entrepreneurs.developersforfuture.org
      http:
        paths:
          - path: /
            backend:
              serviceName: developers-entrepreneurs
              servicePort: 7092
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: developers-entrepreneurs-campaing-vc
  namespace: entrepreneurs
  labels:
    app.kubernetes.io/name: developers-entrepreneurs
    app.kubernetes.io/instance: developers-entrepreneurs-v1
    app.kubernetes.io/version: "v1"
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
  storageClassName: sys11-quobyte
  volumeMode: Filesystem
