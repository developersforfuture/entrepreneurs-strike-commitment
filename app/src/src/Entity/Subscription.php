<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionRepository")
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $companyName = '';
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $contactName = '';
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $contactMail = '';
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $department = '';
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $city = '';
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $address = '';
    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state = '';
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $country = '';
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $verificationToken;
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isDataPolicyAccepted = false;
    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $verificationDate;


    public function toArray(): array
    {
        return [
            $this->id,
            $this->companyName,
            $this->contactName,
            $this->contactMail,
            $this->department,
            $this->city,
            $this->state,
            $this->address,
            $this->country,
            $this->isVerified === false ? 0 : 1,
            $this->verificationToken,
            $this->isDataPolicyAccepted === true ? 1 : 0,
            $this->createdDate instanceof \DateTime ? $this->createdDate->format('Y-m-d H:i:s') : '',
            $this->verificationDate instanceof \DateTime ? $this->verificationDate->format('Y-m-d H:i:s') : '',
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return bool
     */
    public function isVerified()
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return string
     */
    public function getVerificationToken()
    {
        return $this->verificationToken;
    }

    /**
     * @param string $verificationToken
     */
    public function setVerificationToken($verificationToken)
    {
        $this->verificationToken = $verificationToken;
    }

    /**
     * @return bool
     */
    public function isDataPolicyAccepted(): bool
    {
        return $this->isDataPolicyAccepted;
    }

    /**
     * @param bool $isDataPolicyAccepted
     */
    public function setIsDataPolicyAccepted(bool $isDataPolicyAccepted): void
    {
        $this->isDataPolicyAccepted = $isDataPolicyAccepted;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName(string $companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getContactName(): string
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     */
    public function setContactName(string $contactName): void
    {
        $this->contactName = $contactName;
    }

    /**
     * @return string
     */
    public function getContactMail(): string
    {
        return $this->contactMail;
    }

    /**
     * @param string $contactMail
     */
    public function setContactMail(string $contactMail): void
    {
        $this->contactMail = $contactMail;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationDate()
    {
        return $this->verificationDate;
    }

    /**
     * @param \DateTime $verificationDate
     */
    public function setVerificationDate(\DateTime $verificationDate)
    {
        $this->verificationDate = $verificationDate;
    }
}
