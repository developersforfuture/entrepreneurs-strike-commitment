<?php

namespace App\Controller\Campaign;

use App\Entity\Subscription;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class VerificationController extends AbstractController
{
    public function index(string $token, EntityManagerInterface $entityManager, LoggerInterface $logger): Response
    {

        /** @var Subscription $existingSubscription */
        $existingSubscription = $entityManager->getRepository(Subscription::class)
            ->findOneBy(['verificationToken' => $token]);

        if (!$existingSubscription instanceof Subscription && $existingSubscription->isVerified()){
            $logger->info('No subscription found to verify or a second request to verify.');
            return new Response('', Response::HTTP_NOT_FOUND);
        }    

        $existingSubscription->setIsVerified(true);
        $existingSubscription->setVerificationDate(new \DateTime('now'));
        $entityManager->persist($existingSubscription);
        $entityManager->flush();

        $logger->info('Successful subscription verification');
        return $this->render('verification.html.twig', []);
    }
}
