<?php

namespace App\Controller\Campaign;

use App\Entity\Subscription;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class ExportController extends AbstractController
{
    public function index(ObjectManager $manager): Response
    {
        $subscriptions = $manager->getRepository(Subscription::class)->findAll();
        $response = new StreamedResponse();
        $response->setCallback(function() use ($subscriptions) {
            $handle = fopen('php://output', 'w+');

            // Add the header of the CSV file
            fputcsv($handle, [
                'id',
                'company_name',
                'contact_name',
                'contact_mail',
                'department',
                'city',
                'state',
                'address',
                'country',
                'is_verified',
                'verification_token',
                'is_data_policy_accepted',
                'creation_date',
                'verification_date',
            ],';');
            foreach ($subscriptions as $subscription) {
                fputcsv($handle, $subscription->toArray());
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }
}
